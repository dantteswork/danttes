<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller']        = 'welcome';
$route['404_override']              = '';
$route['translate_uri_dashes']      = FALSE;


$route['geral']                     = 'go';
$route['feed']                      = 'go/feed';
$route['dashboard']                      = 'go/feed';

$route['novocurriculo']             = 'go/novocurriculo';
$route['editarcurriculo']           = 'go/editar__curriculo/';
$route['login__curriculo']          = 'go/login';
$route['pre__cadastro__usuario  ']  = 'go/pre__cadastro__usuario';

$route['parceiro']                  = 'go/parceiro';
$route['login__parceiro']           = 'parceiro/login';
$route['pre__cadastro__parceiro']   = 'parceiro/pre__cadastro__parceiro';
$route['cadastro__parceiro']        = 'parceiro/cadastro__parceiro';
$route['dashboard__parceiro']                 = 'parceiro/dashboard';

$route['empresa']                   = 'go/empresa';
$route['login__empresa']            = 'empresa/login__empresa';
$route['pre__cadastro__empresa']    = 'empresa/pre__cadastro__empresa';
$route['vagas']                     = 'empresa/nova__vaga';
$route['cadastro__nova__vaga']      = 'empresa/cadastro__nova__vaga';

$route['credenciado']               = 'credenciado';
$route['cadastro__credenciado']     = 'credenciado/cadastro__credenciado';

$route['freela']                    = 'go/danttes__freela';
$route['login__freela']             = 'freela/login__freela';