<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Credenciado extends CI_Controller {

	public function index()
	{
		$dados['title'] = 'Danttes';
		$dados['titulo'] = 'Danttes';
		$dados['nome'] = 'Aleixo';
		
		$this->load->view('head&footer/head__view', $dados);
		$this->load->view('credenciado/menu__view');
		$this->load->view('credenciado/index__view');
		$this->load->view('head&footer/scripts__view');
	}

	public function cadastro__credenciado()
	{
		$dados['title'] = 'Danttes';
		$dados['titulo'] = 'Danttes';
		$dados['nome'] = 'Aleixo';
		
		$this->load->view('head&footer/head__view', $dados);
		$this->load->view('credenciado/cadastro__view');
		$this->load->view('head&footer/scripts__view');
	}
}
