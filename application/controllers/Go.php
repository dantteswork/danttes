<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Go extends CI_Controller {

	
	public function __construct()
	{
		parent::__construct();
		$user = $this->session->userdata("empresa");

		if(empty($user))
		{
			redirect('login__empresa', 'refresh');
		}
	}
	
	public function index()
	{
		$dados['titulo'] = 'Danttes';
		
		$this->load->view('head&footer/head__view', $dados);
		$this->load->view('index/menu__view');
		$this->load->view('index/index__view');
		$this->load->view('head&footer/scripts__view');
	}

	public function login()
	{
		$dados['title'] = 'Danttes';
		$dados['titulo'] = 'Danttes';

		$this->load->view('head&footer/head__view', $dados);
		$this->load->view('index/login__view');
		$this->load->view('head&footer/scripts__view');
	}

	public function danttes__freela()
	{
		$dados['title'] = 'Danttes Freela';
		$dados['home'] = 'Danttes Freela';
		
		$this->load->view('head&footer/head__view', $dados);
		$this->load->view('danttes__freela/menu__view');
		$this->load->view('danttes__freela/index__view');
		$this->load->view('head&footer/scripts__view');
	}

	public function feed()
	{
		$dados['titulo'] = 'Danttes';
		$dados['nome'] = 'Aleixo';
		
		$this->load->view('head&footer/head__view', $dados);
		$this->load->view('index/feed__novo__curriculo__view');
		$this->load->view('head&footer/scripts__view');
	}

	public function parceiro()
	{
		$dados['title'] = 'Danttes';
		$dados['titulo'] = 'Danttes';
		$dados['nome'] = 'Aleixo';
		
		$this->load->view('head&footer/head__view', $dados);
		$this->load->view('parceiro/menu__view');
		$this->load->view('parceiro/index__view');
		$this->load->view('head&footer/scripts__view');
	}

	public function empresa()
	{
		$dados['title'] = 'Danttes';
		$dados['titulo'] = 'Danttes';
		$dados['nome'] = 'Aleixo';
		
		$this->load->view('head&footer/head__view', $dados);
		$this->load->view('empresa/menu__view');
		$this->load->view('empresa/index__view');
		$this->load->view('head&footer/scripts__view');
	}

	public function novocurriculo()
	{
		$dados['title'] = 'Danttes';
		$dados['titulo'] = 'Danttes';
		$dados['nome'] = 'Aleixo';
		
		$this->load->view('head&footer/head__view', $dados);
		$this->load->view('formularios/novocurriculo__view');
		$this->load->view('head&footer/scripts__view');
	}

	public function test()
	{
		$dados['titulo'] = 'Danttes';
		$dados['nome'] = 'Aleixo';
		
		$this->load->view('head&footer/head__view', $dados);
		$this->load->view('empresa/index__view');
		$this->load->view('head&footer/scripts__view');
	}
	
	public function pre__cadastro__usuario()
	{
		if ($this->input->post('cpf') == NULL)
		{
			echo 'O compo nome do CPF é obrigatório.';
			echo 'Voltar';
		} 
		else 
		{
			//Carrega o Model usuarios				
			$this->load->model('Go__model', 'usuarios');

			//Pega dados do post e guarda na array $dados
			$dados['nome']		 		 = $this->input->post('nome');
			$dados['email']				 = $this->input->post('email');
			$dados['cpf']		 		 = $this->input->post('cpf');
			$dados['senha']				 = md5($this->input->post('senha'));
			$dados['nascimento']		 = $this->input->post('nascimento');

			//Verifica se foi passado via post a id do usuarios
			if ($this->input->post('id') != NULL) {		
				//Se foi passado ele vai fazer atualização no registro.	
				$this->usuarios->editar__curriculo($dados, $this->input->post('id'));

				redirect('../geral/','refresh');
			} 
			
			else {
				//Se Não foi passado id ele adiciona um novo registro
				$this->usuarios->salvar__usuario($dados);
				redirect('feed', 'refresh');
			}
		}
	}
	
	public function cadastro__usuario()
	{
		//Verifica se foi passado o campo nome vazio.
		if ($this->input->post('cpf') == NULL)
		{
			echo 'O compo nome do CPF é obrigatório.';
			echo 'Voltar';
		} 
		else 
		{
			//Carrega o Model usuarios				
			$this->load->model('Go__model', 'usuarios');

			//Pega dados do post e guarda na array $dados
			$dados['nome']		 		 = $this->input->post('nome');
			$dados['cpf']		 		 = $this->input->post('cpf');
			$dados['nascimento']		 = $this->input->post('nascimento');
			$dados['estadocivil']		 = $this->input->post('estadocivil');
			$dados['filhos']			 = $this->input->post('filhos');
			$dados['email']				 = $this->input->post('email');
			$dados['instagram']			 = $this->input->post('instagram');
			$dados['tel1']				 = $this->input->post('tel1');
			$dados['rua']				 = $this->input->post('rua');
			$dados['bairro']			 = $this->input->post('bairro');
			$dados['cidade']			 = $this->input->post('cidade');
			$dados['estado']			 = $this->input->post('estado');
			$dados['cep']				 = $this->input->post('cep');
			$dados['nivel']				 = $this->input->post('nivel');
			$dados['instituicao']		 = $this->input->post('instituicao');
			$dados['curso']				 = $this->input->post('curso');
			$dados['area']				 = $this->input->post('area');
			$dados['ultimaempresa'] 	 = $this->input->post('ultimaempresa');
			$dados['ultimocargo']		 = $this->input->post('ultimocargo');
			$dados['periodo']			 = $this->input->post('periodo');
			$dados['atividades']		 = $this->input->post('atividades');
			$dados['espacolivre']		 = $this->input->post('espacolivre');
			$dados['senha']				 = $this->input->post('senha');
			$dados['confirmarsenha']	 = $this->input->post('confirmarsenha');

			//Verifica se foi passado via post a id do usuarios
			if ($this->input->post('id') != NULL) {		
				//Se foi passado ele vai fazer atualização no registro.	
				$this->usuarios->editar__curriculo($dados, $this->input->post('id'));

				redirect('../geral/','refresh');
			} 
			
			else {
				//Se Não foi passado id ele adiciona um novo registro
				$this->usuarios->salvar__curriculo($dados);
				redirect('../geral/', 'refresh');
			}

			//redicionamento para a página 		
			/* redirect('http://localhost:8888/sigaregistroo/listar'); */
			$this->load->view('head&footer/head__view', $dados);
			$this->load->view('index/index__view');
			$this->load->view('head&footer/scripts__view');
		}
	}
	
	public function editar__curriculo($id=NULL)	
	{
		//Verifica se foi passado um ID, se não vai para a página listar usuarios
		if($id == NULL) 
		{
			echo 'Pfv passe uma ID.';
		}

		$this->load->model('Go__model', 'usuarios');

		$query = $this->usuarios->retornar__usuario($id);

		if($query == NULL) 
		{
			echo 'Nenhum usuário registrado?';	
		}

		//Criamos uma array onde vai guardar os dados do usuario e passamos como parametro para view;	
		$dados['usuarios'] = $query;

		/* echo '<pre>';
			print_r($query); */

		$this->load->view('head&footer/head__view', $dados);
		$this->load->view('formularios/editar__curriculo__view');
		$this->load->view('head&footer/scripts__view');
	}

	
}
