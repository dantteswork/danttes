<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empresa extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$user = $this->session->userdata("empresa");

		if(empty($user))
		{
			redirect('login__empresa', 'refresh');
		}
	}

	public function login__empresa()
	{
		$dados['title'] = 'Danttes';
		$dados['titulo'] = 'Danttes';
		$dados['nome'] = 'Aleixo';
		
		$this->load->view('head&footer/head__view', $dados);
		$this->load->view('empresa/login__view');
		$this->load->view('head&footer/scripts__view');
	}

	public function login()
	{
		$cnpj = $this->input->post("cnpj");
		$senha = md5($this->input->post("senha"));

		$this->db->where('cnpj', $cnpj);
		$this->db->where('senha', $senha);

		$query = $this->db->get('empresa', 1);
	
		if($query->num_rows() == 1)
		{
			$empresa = $query->row();
			$this->session->set_userdata("empresa", $empresa->cnpj);

			redirect('feed','refresh');
		}
		else{
			echo $senha;
		}
	}

	public function pre__cadastro__empresa()
	{
		if ($this->input->post('email') == NULL)
		{
			echo 'O compo nome do CPF é obrigatório.';
			echo 'Voltar';
		} 
		else 
		{
			//Carrega o Model usuarios				
			$this->load->model('Go__model', 'empresa');

			//Pega dados do post e guarda na array $dados
			$dados['nome']		 		 = $this->input->post('nome');
			$dados['email']				 = $this->input->post('email');
			$dados['cidade']		 	 = $this->input->post('cidade');
			$dados['senha']		 		 = md5($this->input->post('senha'));

			//Verifica se foi passado via post a id do usuarios
			if ($this->input->post('id') != NULL) {		
				//Se foi passado ele vai fazer atualização no registro.	
				$this->empresa->editar__curriculo($dados, $this->input->post('id'));

				redirect('../geral/','refresh');
			} 
			
			else {
				//Se Não foi passado id ele adiciona um novo registro
				$this->empresa->pre__cadastro__empresa($dados);
				redirect('dashboard', 'refresh');
			}
		}
	}

	public function nova__vaga()
	{
		$dados['title'] = 'Danttes';
		$dados['titulo'] = 'Danttes';
		$dados['nome'] = 'Aleixo';
		
		$dados['nomeempresa'] = 'Motorac';
		$dados['cidadecedeunidade'] = 'Caruaru';
		$dados['estadocedeunidade'] = 'PE';
		
		$this->load->view('head&footer/head__view', $dados);
		$this->load->view('empresa/menu__view');
		$this->load->view('empresa/nova__vaga__view');
		$this->load->view('head&footer/scripts__view');
	}

	public function cadastro__nova__vaga()
	{
		if ($this->input->post('nomeempresa') == NULL)
		{
			echo 'O compo nome da empresa é obrigatório.';
			echo 'Voltar';
		} 
		else 
		{
			//Carrega o Model usuarios				
			$this->load->model('Go__model', 'nova__vaga');

			//Pega dados do post e guarda na array $dados
			$dados['nomeempresa']		 		 = $this->input->post('nomeempresa');
			$dados['cidade']					 = $this->input->post('cidade');
			$dados['titulodaoportunidade']		 = $this->input->post('titulodaoportunidade');
			$dados['descricaooportunidade'] 	 = $this->input->post('descricaooportunidade');
			$dados['beneficios']		 		 = $this->input->post('beneficios');
			$dados['prerequisito']		 		 = $this->input->post('prerequisito');
			$dados['cnh']		 		 = $this->input->post('cnh');
			$dados['remunecarao']		 		 = $this->input->post('remunecarao');

			//Verifica se foi passado via post a id do usuarios
			if ($this->input->post('id') != NULL) {		
				//Se foi passado ele vai fazer atualização no registro.	
				$this->nova__vaga->editar__curriculo($dados, $this->input->post('id'));

				redirect('../geral/','refresh');
			} 
			
			else {
				//Se Não foi passado id ele adiciona um novo registro
				$this->nova__vaga->cadastro__nova__vaga($dados);
				redirect('feed', 'refresh');
			}
		}
	}
}
