<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parceiro extends CI_Controller {

	public function login()
	{
		$dados['title'] = 'Danttes';
		$dados['titulo'] = 'Danttes';

		$this->load->view('head&footer/head__view', $dados);
		$this->load->view('parceiro/login__view');
		$this->load->view('head&footer/scripts__view');
	}

	public function dashboard()
	{
		$dados['title'] = 'Danttes';
		$dados['titulo'] = 'Danttes';

		$this->load->view('head&footer/head__view', $dados);
		$this->load->view('parceiro/dashboard__parceiro__view');
		$this->load->view('head&footer/scripts__view');
	}

	public function cadastro__parceiro()
	{
		$dados['title'] = 'Danttes';
		$dados['titulo'] = 'Danttes';

		$this->load->view('head&footer/head__view', $dados);
		$this->load->view('parceiro/cadastro__view');
		$this->load->view('head&footer/scripts__view');
	}

	public function pre__cadastro__parceiro()
	{
		//Verifica se foi passado o campo nome vazio.
		if ($this->input->post('email') == NULL)
		{
			echo 'O compo nome Email é obrigatório.';
			echo 'Voltar';
		} 
		else 
		{
			//Carrega o Model usuarios				
			$this->load->model('Go__model', 'parceiro');

			//Pega dados do post e guarda na array $dados
			$dados['nome']		 		 = $this->input->post('nome');
			$dados['email']		 		 = $this->input->post('email');
			$dados['senha']				 = $this->input->post('senha');

			//Verifica se foi passado via post a id do usuarios
			if ($this->input->post('id') != NULL) {		
				//Se foi passado ele vai fazer atualização no registro.	
				$this->parceiro->atualizar__cadastro__parceiro($dados, $this->input->post('id'));

				redirect('../login/','refresh');
			} 
			
			else {
				//Se Não foi passado id ele adiciona um novo registro
				$this->parceiro->pre__cadastro__parceiro($dados);
				redirect('feed', 'refresh');
			}
		}
	}

}
