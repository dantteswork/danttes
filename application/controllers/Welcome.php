<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$dados['titulo'] = 'Danttes';
		$dados['title'] = 'Danttes';
		
		$this->load->view('head&footer/head__view', $dados);
		$this->load->view('index/menu__view');
		$this->load->view('index/index__view');
		$this->load->view('head&footer/scripts__view');
	}
}
