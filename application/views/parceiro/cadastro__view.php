<body data-spy="scroll" data-target=".onpage-navigation" data-offset="60">
  <div class="page-loader">
    <div class="loader">Loading...</div>
  </div>
  <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
    <div class="container">
    <div class="navbar-header">
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="navbar-brand" href="index.html"><?php echo $titulo?></a>
      </div>
      <div class="collapse navbar-collapse" id="custom-collapse">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="#totop">Home</a></li>
          <li><a class="section-scroll" href="#services">Porque me cadastrar?</a></li>
          <li><a class="section-scroll" href="#about">Sobre</a></li>
          <li><a class="section-scroll" href="#news">Dicas e Insights</a></li>
          <li><a class="section-scroll" href="#contact">Segurança</a></li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="main">
    <section class="module">
      <div class="container">
        <div class="row">
          <div class="col-sm-9 col-sm-offset-2">
            <div role="tabpanel">
              <ul class="nav nav-tabs font-alt" role="tablist">
                <li class="active"><a href="#dadospessoais" data-toggle="tab"><span class="icon-document"></span>Dados Pessoais</a></li>
                <li><a href="#endereco" data-toggle="tab"><span class=" icon-envelope"></span>Endereço</a></li>
                <li><a href="#escolaridade" data-toggle="tab"><span class=" icon-notebook"></span>Escolaridade</a></li>
                <li><a href="#historicotrabalhista" data-toggle="tab"><span class="icon-book-open"></span>Histórico Trabalhista</a></li>
                <li><a href="#conta" data-toggle="tab"><span class="icon-lock"></span>Conta</a></li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="dadospessoais">
                  <form action="../index/go/salvar__curriculo/" method="post">
                    <div class="panel-group">
                      <div class="col-md-6 col-lg-6">
                        <div class="post">
                          <div class="form-group">
                            <label for="">Nome</label>
                            <input class="form-control input-lg" type="text" name="nome" placeholder="Nome Completo" required>
                          </div>
                          <div class="form-group">
                            <label for="">CPF</label>
                            <input class="form-control input-lg" type="text" name="cpf" placeholder="CPF" required>
                          </div>
                          <div class="form-group">
                            <label for="">Data de Nascimento</label>
                            <input class="form-control input-lg" type="date" name="nascimento" placeholder="Data de Nascimento" required>
                          </div>
                          <div class="form-group">
                            <label for="">Estado Civil</label>
                            <select class="form-control input-lg" name="estadocivil">
                              <option value="0">-----</option>
                              <option value="1">Solteiro</option>
                              <option value="2">Casado</option>
                              <option value="3">Viúvo</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6 col-lg-6">
                        <div class="post">
                          <div class="form-group">
                            <label for="">Filhos</label>
                            <select class="form-control input-lg" name="filhos">
                              <option value="1">Sim</option>
                              <option value="0">Não</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="">Instagram</label>
                            <input class="form-control input-lg" type="text" name="instagram" placeholder="Instagram">
                          </div>
                          <div class="form-group">
                            <label for="">Email</label>
                            <input class="form-control input-lg" type="text" name="email" placeholder="E-mail">
                          </div>
                          <div class="form-group">
                            <label for="">Telefone 1</label>
                            <input class="form-control input-lg" type="number" name="tel1" placeholder="Telefone">
                          </div>
                          <!-- <div class="form-group">
                            <label for="">Telefone 2</label>
                            <input class="form-control input-lg" type="text" name="tel2" placeholder="Phone Number">
                          </div> -->
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane" id="endereco">
                    <div class="panel-group">
                      <div class="col-md-6 col-lg-6">
                        <div class="post">
                          <div class="form-group">
                            <label for="">Rua</label>
                            <input class="form-control input-lg" type="text" name="rua" placeholder="Rua">
                          </div>
                          <div class="form-group">
                            <label for="">Bairro</label>
                            <input class="form-control input-lg" type="text" name="bairro" placeholder="Bairro">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6 col-lg-6">
                        <div class="post">
                          <div class="form-group">
                            <label for="">Cidade</label>
                            <input class="form-control input-lg" type="text" name="cidade" placeholder="Cidade">
                          </div>
                          <div class="form-group">
                            <label for="">Estado</label>
                            <input class="form-control input-lg" type="text" name="estado" placeholder="Estado">
                          </div>
                          <div class="form-group">
                            <label for="">CEP</label>
                            <input class="form-control input-lg" type="number" name="cep" placeholder="CEP">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane" id="escolaridade">
                    <div class="panel-group">
                      <div class="col-md-6 col-lg-6">
                        <div class="post">
                          <div class="form-group">
                            <label for="">Nível</label>
                            <select class="form-control input-lg" name="nivel">
                              <option value="1">Fundamental Incompleto</option>
                              <option value="2">Fundamental Completo</option>
                              <option value="3">Médio Incompleto</option>
                              <option value="4">Médio Completo</Command></option>
                              <option value="5">Superior Incompleto</option>
                              <option value="6">Superior Completo</option>
                              <option value="7">Mestrado Incompleto</option>
                              <option value="8">Pós-graduação Completo</option>
                              <option value="9">Pós-graduação Incompleto</option>
                              <option value="10">Mestrado Completo</option>
                              <option value="11">Mestrado Incompleto</option>
                              <option value="12">Doutorado Incompleto</option>
                              <option value="13">Doutorado Completo</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="">Instituição de Ensino</label>
                            <input class="form-control input-lg" type="text" name="instituicao" placeholder="Instituição de Ensino">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6 col-lg-6">
                        <div class="post">
                          <div class="form-group">
                            <label for="">Curso</label>
                            <input class="form-control input-lg" type="text" name="curso" placeholder="Curso">
                          </div>
                          <div class="form-group">
                            <label for="">Área de Atuação</label>
                            <input class="form-control input-lg" type="text" name="area" placeholder="Área de Atuação">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane" id="historicotrabalhista">
                    <div class="panel-group">
                      <div class="col-md-6 col-lg-6">
                        <div class="post">
                          <div class="form-group">
                            <label for="">Última Empresa</label>
                            <input class="form-control input-lg" type="text" name="ultimaempresa" placeholder="Última Empresa">
                          </div>
                          <div class="form-group">
                            <label for="">Último Cargo</label>
                            <input class="form-control input-lg" type="text" name="ultimocargo" placeholder="Último Cargo">
                          </div>
                          <div class="form-group">
                            <label for="">Periodo</label>
                            <input class="form-control input-lg" type="text" name="periodo" placeholder="Periodo">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6 col-lg-6">
                        <div class="post">
                          <div class="form-group">
                            <label for="">Atividades</label>
                            <textarea class="form-control input-lg" name="atividades" id="" cols="30" rows="4"></textarea>
                          </div>
                          <div class="form-group">
                            <label for="">Use esse espaço para in</label>
                            <textarea class="form-control input-lg" name="espacolivre" id="" cols="30" rows="4" placeholder="Apresente-se"></textarea>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane" id="conta">
                    <div class="panel-group">
                      <div class="col-md-6 col-lg-6">
                        <div class="post">
                          <div class="form-group">
                            <label for="">Senha</label>
                            <input class="form-control input-lg" type="password" name="senha" placeholder="Senha" required>
                          </div>
                          <div class="form-group">
                            <label for="">Confirmar Senha</label>
                            <input class="form-control input-lg" type="password" name="confirmarsenha" placeholder="Confirmar Senha" required>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6 col-lg-6">
                        <div class="post">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <button class="btn btn-b" type="submit" style="margin-left:10%;">Salvar</button>&nbsp;
            </form>
          </div>
        </div>
      </div>
    </section>
    <hr class="divider-d">
    <footer class="footer bg-dark">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <p class="copyright font-alt">&copy; 2017&nbsp;<a href="index.html">TitaN</a>, All Rights Reserved</p>
          </div>
          <div class="col-sm-6">
            <div class="footer-social-links"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-dribbble"></i></a><a href="#"><i class="fa fa-skype"></i></a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </div>
  <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a></div>
</main>