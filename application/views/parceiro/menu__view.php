<body data-spy="scroll" data-target=".onpage-navigation" data-offset="60">
    <main>
      <nav class="navbar navbar-custom navbar-transparent navbar-fixed-top one-page" role="navigation">
        <div class="container">
          <div class="navbar-header">
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="navbar-brand" href="#totop"><?php echo $titulo?></a>
          </div>
          <div class="collapse navbar-collapse" id="custom-collapse">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="#totop">Home</a></li>
              <li><a class="section-scroll" href="#services">Porque me cadastrar?</a></li>
              <li><a class="section-scroll" href="#about">Sobre</a></li>
              <li><a class="section-scroll" href="#news">Dicas e Insights</a></li>
              <li><a class="section-scroll" href="#contact">Contato</a></li>
              <li><a class="section-scroll">|</a></li>
              <li><a class="section-scroll" href="login__parceiro"><strong>login / Registre-se</strong></a></li>
            </ul>
          </div>
        </div>
      </nav>