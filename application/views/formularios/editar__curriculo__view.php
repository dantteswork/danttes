<body data-spy="scroll" data-target=".onpage-navigation" data-offset="60">
      <div class="page-loader">
        <div class="loader">Loading...</div>
      </div>
      <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="navbar-brand" href="index.html"><?php echo $titulo?></a>
          </div>
          <div class="collapse navbar-collapse" id="custom-collapse">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="#totop">Home</a></li>
              <li><a class="section-scroll" href="#services">Porque me cadastrar?</a></li>
              <li><a class="section-scroll" href="#about">Sobre</a></li>
              <li><a class="section-scroll" href="#news">Dicas e Insights</a></li>
              <li><a class="section-scroll" href="#contact">Contato</a></li>
            </ul>
          </div>
        </div>
      </nav>
      <div class="main">
        <section class="module bg-dark-60 faq-page-header" data-background="https://images.unsplash.com/photo-1486312338219-ce68d2c6f44d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=74be18f074e19e06a51221f0f09969df&auto=format&fit=crop&w=1052&q=80">
          <div class="container">
            <div class="row">
              <div class="col-sm-6 col-sm-offset-3">
                <h2 class="module-title font-alt">Faq</h2>
                <div class="module-subtitle font-serif">A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</div>
              </div>
            </div>
          </div>
        </section>
        <section class="module">
          <div class="container">
            <div class="row">
              <div class="col-sm-10 col-sm-offset-1">
                <div role="tabpanel">
                  <ul class="nav nav-tabs font-alt" role="tablist">
                    <li class="active"><a href="#dadospessoais" data-toggle="tab"><span class="icon-document"></span>Dados Pessoais</a></li>
                    <li><a href="#endereco" data-toggle="tab"><span class=" icon-envelope"></span>Endereço</a></li>
                    <li><a href="#escolaridade" data-toggle="tab"><span class=" icon-notebook"></span>Escolaridade</a></li>
                    <li><a href="#historicotrabalhista" data-toggle="tab"><span class="icon-book-open"></span>Histórico Trabalhista</a></li>
                    <li><a href="#conta" data-toggle="tab"><span class="icon-lock"></span>Conta</a></li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane active" id="dadospessoais">
                      <form action="../../../index/go/salvar__curriculo/" method="post">
                        <div class="panel-group">
                          <div class="col-md-6 col-lg-6">
                            <div class="post">
                              <div class="form-group">
                                <label for="">Nome</label>
                                <input class="form-control input-lg" type="text" name="nome" value="<?php echo $usuarios->nome?>" placeholder="Nome Completo">
                              </div>
                              <div class="form-group">
                                <label for="">CPF</label>
                                <input class="form-control input-lg" type="text" name="cpf" value="<?php echo $usuarios->cpf?>" placeholder="CPF">
                              </div>
                              <div class="form-group">
                                <label for="">Data de Nascimento</label>
                                <input class="form-control input-lg" type="date" value="<?php echo $usuarios->nascimento?>" name="nascimento" placeholder="Data de Nascimento">
                              </div>
                              <div class="form-group">
                                <label for="">Estado Civil</label>
                                <select class="form-control input-lg" name="estadocivil">
                                  <option value="0">-----</option>
                                  <option value="1" <option value="0" <?php echo ($usuarios->estadocivil == 1 ? 'selected=selected' : '')?>>Solteiro</option>
                                  <option value="2" <option value="0" <?php echo ($usuarios->estadocivil == 2 ? 'selected=selected' : '')?>>Casado</option>
                                  <option value="3" <option value="0" <?php echo ($usuarios->estadocivil == 3 ? 'selected=selected' : '')?>>Viuvo</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6 col-lg-6">
                            <div class="post">
                              <div class="form-group">
                                <label for="">Filhos</label>
                                <select class="form-control input-lg" name="filhos">
                                <option value="0" <?php echo ($usuarios->filhos == 0 ? 'selected=selected' : '')?>>Não</option>
                                <option value="1" <?php echo ($usuarios->filhos == 1 ? 'selected=selected' : '')?>>Sim</option>
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="">Instagram</label>
                                <input class="form-control input-lg" type="text" value="<?php echo $usuarios->instagram?>" name="instagram" placeholder="Instagram">
                              </div>
                              <div class="form-group">
                                <label for="">Email</label>
                                <input class="form-control input-lg" type="text" value="<?php echo $usuarios->email?>" name="email" placeholder="E-mail">
                              </div>
                              <div class="form-group">
                                <label for="">Telefone 1</label>
                                <input class="form-control input-lg" type="number" value="<?php echo $usuarios->tel1?>" name="tel1" placeholder="Telefone">
                              </div>
                              <!-- <div class="form-group">
                                <label for="">Telefone 2</label>
                                <input class="form-control input-lg" type="text" name="tel2" placeholder="Phone Number">
                              </div> -->
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane" id="endereco">
                        <div class="panel-group">
                          <div class="col-md-6 col-lg-6">
                            <div class="post">
                              <div class="form-group">
                                <label for="">Rua</label>
                                <input class="form-control input-lg" type="text" value="<?php echo $usuarios->rua?>" name="rua" placeholder="Rua">
                              </div>
                              <div class="form-group">
                                <label for="">Bairro</label>
                                <input class="form-control input-lg" type="text" value="<?php echo $usuarios->bairro?>" name="bairro" placeholder="Bairro">
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6 col-lg-6">
                            <div class="post">
                              <div class="form-group">
                                <label for="">Cidade</label>
                                <input class="form-control input-lg" type="text" value="<?php echo $usuarios->cidade?>" name="cidade" placeholder="Cidade">
                              </div>
                              <div class="form-group">
                                <label for="">Estado</label>
                                <input class="form-control input-lg" type="text" value="<?php echo $usuarios->estado?>" name="estado" placeholder="Estado">
                              </div>
                              <div class="form-group">
                                <label for="">CEP</label>
                                <input class="form-control input-lg" type="number" value="<?php echo $usuarios->cep?>" name="cep" placeholder="CEP">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane" id="escolaridade">
                        <div class="panel-group">
                          <div class="col-md-6 col-lg-6">
                            <div class="post">
                              <div class="form-group">
                                <label for="">Nível</label>
                                <select class="form-control input-lg" name="nivel">
                                  <option value="1">Fundamental Incompleto</option>
                                  <option value="2">Fundamental Completo</option>
                                  <option value="3">Médio Incompleto</option>
                                  <option value="4">Médio Completo</Command></option>
                                  <option value="5">Superior Incompleto</option>
                                  <option value="6">Superior Completo</option>
                                  <option value="7">Mestrado Incompleto</option>
                                  <option value="8">Pós-graduação Completo</option>
                                  <option value="9">Pós-graduação Incompleto</option>
                                  <option value="10">Mestrado Completo</option>
                                  <option value="11">Mestrado Incompleto</option>
                                  <option value="12">Doutorado Incompleto</option>
                                  <option value="13">Doutorado Completo</option>
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="">Instituição de Ensino</label>
                                <input class="form-control input-lg" type="text" value="<?php echo $usuarios->instituicao?>" name="instituicao" placeholder="Instituição de Ensino">
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6 col-lg-6">
                            <div class="post">
                              <div class="form-group">
                                <label for="">Curso</label>
                                <input class="form-control input-lg" type="text" value="<?php echo $usuarios->curso?>" name="curso" placeholder="Curso">
                              </div>
                              <div class="form-group">
                                <label for="">Área de Atuação</label>
                                <input class="form-control input-lg" type="text" value="<?php echo $usuarios->area?>" name="area" placeholder="Área de Atuação">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane" id="historicotrabalhista">
                        <div class="panel-group">
                          <div class="col-md-6 col-lg-6">
                            <div class="post">
                              <div class="form-group">
                                <label for="">Última Empresa</label>
                                <input class="form-control input-lg" type="text" value="<?php echo $usuarios->ultimaempresa?>" name="ultimaempresa" placeholder="Última Empresa">
                              </div>
                              <div class="form-group">
                                <label for="">Último Cargo</label>
                                <input class="form-control input-lg" type="text" value="<?php echo $usuarios->ultimocargo?>" name="ultimocargo" placeholder="Último Cargo">
                              </div>
                              <div class="form-group">
                                <label for="">Periodo</label>
                                <input class="form-control input-lg" type="text" value="<?php echo $usuarios->periodo?>" name="periodo" placeholder="Periodo">
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6 col-lg-6">
                            <div class="post">
                              <div class="form-group">
                                <label for="">Atividades</label>
                                <textarea class="form-control input-lg" value="<?php echo $usuarios->atividades?>" name="atividades" id="" cols="30" rows="4"></textarea>
                              </div>
                              <div class="form-group">
                                <label for="">Use esse espaço para in</label>
                                <textarea class="form-control input-lg" value="<?php echo $usuarios->espacolivre?>" name="espacolivre" id="" cols="30" rows="4" placeholder="Apresente-se"></textarea>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane" id="conta">
                        <div class="panel-group">
                          <div class="col-md-6 col-lg-6">
                            <div class="post">
                              <div class="form-group">
                                <label for="">Senha</label>
                                <input class="form-control input-lg" type="password" value="<?php echo $usuarios->senha?>" name="senha" placeholder="Senha">
                              </div>
                              <div class="form-group">
                                <label for="">Confirmar Senha</label>
                                <input class="form-control input-lg" type="password" value="<?php echo $usuarios->confirmarsenha?>" name="confirmarsenha" placeholder="Confirmar Senha">
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6 col-lg-6">
                            <div class="post">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="id" value="<?php $usuarios->id?>">
                  <button class="btn btn-b" type="submit">Salvar</button>
                </form>
              </div>
            </div>
          </div>
        </section>
        <hr class="divider-d">
        <footer class="footer bg-dark">
          <div class="container">
            <div class="row">
              <div class="col-sm-6">
                <p class="copyright font-alt">&copy; 2017&nbsp;<a href="index.html">TitaN</a>, All Rights Reserved</p>
              </div>
              <div class="col-sm-6">
                <div class="footer-social-links"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-dribbble"></i></a><a href="#"><i class="fa fa-skype"></i></a>
                </div>
              </div>
            </div>
          </div>
        </footer>
      </div>
      <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a></div>
    </main>