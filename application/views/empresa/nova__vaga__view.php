<section class="module pt-0 pb-0" id="login">
    <div class="row position-relative m-0">
        <div class="col-xs-12 col-md-3 side-image bg-gradient" data-background="https://images.unsplash.com/photo-1495681956486-6e7977e33d13?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=cca857c21c439182119b51b62b9bb5aa&auto=format&fit=crop&w=1050&q=80"></div>
        <div class="col-xs-12 col-md-9 col-md-offset-3 side-image-text">
        <form action="cadastro__nova__vaga" method="post">
            <div class="row" style="padding:15px;">
                <h3 class="font-alt" style="margin-top: 0px;">Danttes Vagas</h3>
                <p>Divulgue para os melhores talentos da região suas oportunidades de trabalho.</p>
                <hr class="divider-w mt-10 mb-20">
                <div class="col-sm-6">
                    <h3 class="font-alt mb-0" style="margin-top: 0px; padding:10px 10px 10px 0;">Registro de nova vaga</h3>
                    <!-- <div class="font-alt">Create a stylish App Landing Page<br>And highlight all the great things of your product!</div> -->
                    <form action="pre__cadastro__empresa" method="post">
                    <div class="form-group">
                        <label for="" class="font-alt mb-0">Nome da Empresa</label>
                        <input class="form-control input-lg" name="nomeempresa" type="text" value="<?php echo $nomeempresa?>" placeholder="Digite o título da oportunidade."/>
                    </div>
                    <div class="form-group">
                        <label for="" class="font-alt mb-0">Cidade</label>
                        <input class="form-control input-lg" name="cidade" type="text" value="<?php echo $cidadecedeunidade?>" placeholder="Digite o título da oportunidade."/>
                    </div>
                    <div class="form-group">
                        <label for="" class="font-alt mb-0">Titulo da Oportunidade</label>
                        <input class="form-control input-lg" name="titulodaoportunidade" type="text" placeholder="Digite o título da oportunidade."/>
                    </div>
                    <div class="form-group">
                        <label for="" class="font-alt mb-0">Descrição da Oportunidade</label>
                        <textarea class="form-control input-lg" name="descricaooportunidade" rows="7" placeholder="Digite a descrição das principais atividades que o candidato deverá atuar."></textarea>
                    </div>
                </div>
                <div class="col-sm-6">
                    <h3 class="font-alt mb-0" style="margin-top: 0px; padding:10px 10px 10px 0;">:))</h3>
                    <div class="form-group">
                        <label for="" class="font-alt mb-0">Beneficios</label>
                        <input class="form-control input-lg" name="beneficios" type="text" placeholder="Digite os benefícios da empresa."/>
                    </div>
                    <div class="form-group">
                        <label for="" class="font-alt mb-0">Remuneração</label>
                        <input class="form-control input-lg"  name="remunecarao" type="text" placeholder="Digite o valor da remuneração."/>
                    </div>
                    <div class="form-group">
                        <label for="" class="font-alt mb-0">Pre requisito sexo</label>
                        <select class="form-control input-lg" name="prerequisito">
                            <option value="ambos">Ambos</option>
                            <option value="mulher">Mulher</option>
                            <option value="homem">Homem</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="" class="font-alt mb-0">Habilitação CNH</label>
                        <select class="form-control input-lg" name="cnh">
                            <option value="ab">A-B</option>
                            <option value="a">A</option>
                            <option value="b">B</option>
                        </select>
                    </div>
                    <input type="hidden" name="id">
                    <input class="btn btn-info btn-round" type="submit">
                </div>
            </form>
        </div>
    </div>
</section>