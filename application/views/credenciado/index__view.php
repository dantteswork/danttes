      <!-- <section class="home-section home-full-height bg-dark bg-gradient" id="home" data-background="https://images.unsplash.com/photo-1483808161634-29aa1b0e585e?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=3843a39b76d12eee7152f696fb01ba43&auto=format&fit=crop&w=1050&q=80">
        <div class="titan-caption">
            <div class="font-alt mb-30 titan-title-size-1">Está procurando uma nova oportunidade de mudar de emprego?</div>
          <div class="caption-content">
            <div class="font-alt mb-40 titan-title-size-3"> Cadastre seu currículo agora | e apareça para as melhores | empresas da sua região.</span>
            </div><a class="section-scroll btn btn-border-w btn-circle" href="../novocurriculo/">Cadastre-se aqui</a>
          </div>
        </div>
      </section> -->
      <section class="home-section home-parallax home-fade home-full-height" id="home">
        <div class="hero-slider">
          <ul class="slides">
            <li class="bg-dark-30 bg-dark bg-gradient" style="background-image:url(https://images.unsplash.com/photo-1532635241-17e820acc59f?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=3eb43b897f17dfd077c53caaef4c14ed&auto=format&fit=crop&w=1003&q=80);">
              <div class="titan-caption">
                <div class="caption-content" style="padding-top: 10%;">
                  <div class="font-alt mb-30 titan-title-size-3" style="line-height:1.2; padding: 20px;">Use a <strong>barra de pesquisa</strong> a baixo para encontrar parceiros em sua região!</div>
                  <div class="font-alt mb-40 titan-title-size-2"> 
                  <div class="form-group col-sm-12 col-xs-12">
                    <div class="form-group">
                      <input class="form-control-banner input-lg-banner" type="text" placeholder="Digite o que deseja encontra . . ">
                    </div>    
                  </div>
                  </span>
                  </div><a class="section-scroll btn btn-border-w " href="login__curriculo">Agora vamos juntos encontrar os melhores descontos para você</a></div>
              </div>
            </li>
            <!-- <li class="bg-dark-30 bg-dark bg-gradient" style="background-image:url(https://images.unsplash.com/photo-1532987625322-5949307b5bc4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=ea5891af7616841ecb228216643d4308&auto=format&fit=crop&w=1050&q=80);">
              <div class="titan-caption">
                <div class="caption-content">
                    <div class="font-alt mb-30 titan-title-size-3" style="line-height:1.2;">Mas e você?<div>
                    <div class="font-alt mb-30 titan-title-size-3">está precisando de <strong>novos</strong> clientes?</div>
                    <div class="font-alt mb-40 titan-title-size-2" style="font-size:25px; line-height:1.8;"> Conheça seus serviços na plataforma e tenha acesso a mais de  1300 novos leads.</span>
                    </div><a class="section-scroll btn btn-border-w btn-circle" href="parceiro">Conheça a plataforma</a>
                </div>
              </div>
            </li>
            <li class="bg-dark-30 bg-dark bg-gradient" style="background-image:url(https://images.unsplash.com/photo-1525422847952-7f91db09a364?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=ce6622924dae3b9be067e1778a6b8707&auto=format&fit=crop&w=1065&q=80);">
              <div class="titan-caption">
                <div class="caption-content">
                  <div class="font-alt mb-30 titan-title-size-3" style="line-height:1.2;">E a sua <strong>empresa?</strong></div>
                  <div class="font-alt mb-30 titan-title-size-3" style="line-height:1.2;">Já pensou em fornecer <strong>novos benefícios</strong> para seus funcionarios?</div>
                  <div class="font-alt mb-40 titan-title-size-2" style="font-size:25px; line-height:1.8;">Alguma coisa aqui enfatizando o porque que ele devia comprar o serviço.</span>
                  </div><a class="section-scroll btn btn-border-w btn-circle" href="empresa">Conheça nossos benefícios</a></div>
              </div>
            </li> -->
          </ul>
        </div>
      </section>
      <div class="main">
      <section class="module" id="services">
          <div class="container">
            <div class="row">
              <div class="col-sm-6 col-sm-offset-3">
                <h2 class="module-title font-alt">Our Services</h2>
                <div class="module-subtitle font-serif">A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</div>
              </div>
            </div>
            <div class="row multi-columns-row">
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="features-item">
                  <div class="features-icon"><span class="icon-lightbulb"></span></div>
                  <h3 class="features-title font-alt">Ideas and concepts</h3>
                  <p>Careful attention to detail and clean, well structured code ensures a smooth user experience for all your visitors.</p>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="features-item">
                  <div class="features-icon"><span class="icon-bike"></span></div>
                  <h3 class="features-title font-alt">Optimised for speed</h3>
                  <p>Careful attention to detail and clean, well structured code ensures a smooth user experience for all your visitors.</p>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="features-item">
                  <div class="features-icon"><span class="icon-tools"></span></div>
                  <h3 class="features-title font-alt">Designs &amp; interfaces</h3>
                  <p>Careful attention to detail and clean, well structured code ensures a smooth user experience for all your visitors.</p>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="features-item">
                  <div class="features-icon"><span class="icon-gears"></span></div>
                  <h3 class="features-title font-alt">Highly customizable</h3>
                  <p>Careful attention to detail and clean, well structured code ensures a smooth user experience for all your visitors.</p>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="features-item">
                  <div class="features-icon"><span class="icon-tools-2"></span></div>
                  <h3 class="features-title font-alt">Coding &amp; development</h3>
                  <p>Careful attention to detail and clean, well structured code ensures a smooth user experience for all your visitors.</p>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="features-item">
                  <div class="features-icon"><span class="icon-genius"></span></div>
                  <h3 class="features-title font-alt">Features &amp; plugins</h3>
                  <p>Careful attention to detail and clean, well structured code ensures a smooth user experience for all your visitors.</p>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="features-item">
                  <div class="features-icon"><span class="icon-mobile"></span></div>
                  <h3 class="features-title font-alt">Responsive design</h3>
                  <p>Careful attention to detail and clean, well structured code ensures a smooth user experience for all your visitors.</p>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="features-item">
                  <div class="features-icon"><span class="icon-lifesaver"></span></div>
                  <h3 class="features-title font-alt">Dedicated support</h3>
                  <p>Careful attention to detail and clean, well structured code ensures a smooth user experience for all your visitors.</p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="module pt-0 pb-0" id="about">
          <div class="row position-relative m-0">
            <div class="col-xs-12 col-md-6 side-image" data-background="https://images.unsplash.com/photo-1515378791036-0648a3ef77b2?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=cbe45d5a16eeb2c8009b0959f15db36a&auto=format&fit=crop&w=1050&q=80" style="background-image: url(&quot;assets/images/section-14.jpg&quot;);"></div>
            <div class="col-xs-12 col-md-6 col-md-offset-6 side-image-text">
              <div class="row">
                <div class="col-sm-12">
                  <h2 class="module-title font-alt align-left">About Us</h2>
                  <div class="module-subtitle font-serif align-left">A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</div>
                  <p>The European languages are members of the same family. Their separate existence is a myth. For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ in their grammar, their pronunciation and their most common words.</p>
                  <p>The European languages are members of the same family. Their separate existence is a myth. For science, music, sport, etc, Europe uses the same vocabulary.</p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="module" id="news">
          <div class="container">
            <div class="row">
              <div class="col-sm-6 col-sm-offset-3">
                <h2 class="module-title font-alt">Dicas e Insights</h2>
                <div class="module-subtitle font-serif">A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</div>
              </div>
            </div>
            <div class="row multi-columns-row post-columns">
              <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="post mb-20">
                  <div class="post-thumbnail"><a href="#"><img src="https://images.unsplash.com/photo-1434030216411-0b793f4b4173?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=349f0586d07c10fdf29da504276b5407&auto=format&fit=crop&w=1050&q=80" alt="Blog-post Thumbnail"/></a></div>
                  <div class="post-header font-alt">
                    <h2 class="post-title"><a href="#">Our trip to the Alps</a></h2>
                    <div class="post-meta">By&nbsp;<a href="#">Mark Stone</a>&nbsp;| 23 November | 3 Comments
                    </div>
                  </div>
                  <div class="post-entry">
                    <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
                  </div>
                  <div class="post-more"><a class="more-link" href="#">Read more</a></div>
                </div>
              </div>
              <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="post mb-20">
                  <div class="post-thumbnail"><a href="#"><img src="https://images.unsplash.com/photo-1457904375453-3e1fc2fc76f4?ixlib=rb-0.3.5&s=fa35e008ee5b6d0054ece52f5316b4e8&auto=format&fit=crop&w=1050&q=80" alt="Blog-post Thumbnail"/></a></div>
                  <div class="post-header font-alt">
                    <h2 class="post-title"><a href="#">Shore after the tide</a></h2>
                    <div class="post-meta">By&nbsp;<a href="#">Andy River</a>&nbsp;| 11 November | 4 Comments
                    </div>
                  </div>
                  <div class="post-entry">
                    <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
                  </div>
                  <div class="post-more"><a class="more-link" href="#">Read more</a></div>
                </div>
              </div>
              <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="post mb-20">
                  <div class="post-thumbnail"><a href="#"><img src="https://images.unsplash.com/photo-1526289996912-783343505163?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=caf09c9b712ccc9806aba647cf50a367&auto=format&fit=crop&w=1050&q=80" alt="Blog-post Thumbnail"/></a></div>
                  <div class="post-header font-alt">
                    <h2 class="post-title"><a href="#">We in New Zealand</a></h2>
                    <div class="post-meta">By&nbsp;<a href="#">Dylan Woods</a>&nbsp;| 5 November | 15 Comments
                    </div>
                  </div>
                  <div class="post-entry">
                    <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
                  </div>
                  <div class="post-more"><a class="more-link" href="#">Read more</a></div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="module bg-dark-60 request-cta" id="contact" data-background="https://images.unsplash.com/photo-1527067669193-6a36380de229?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=075d07fb2cee915ef07dee0d73cbdd54&auto=format&fit=crop&w=636&q=80" style="background-image: url(&quot;assets/images/finance/rqst_bg.jpg&quot;);">
          <div class="container">
            <div class="row">
              <div class="col-sm-4">
                <h2 class="font-alt">Request a call back</h2>
                <p>Would you like to speak to one of our financial advisers over the phone? Just submit your details and we’ll be in touch shortly. You can also email us if you would prefer.</p>
              </div>
              <div class="col-sm-8">
                <div class="row">
                  <form class="form rqst-form" id="requestACall" role="form" method="post" action="php/request_call.php">
                    <div class="form-group col-sm-6 col-xs-12">
                      <input class="form-control input-lg" type="text" name="name" placeholder="Name">
                    </div>
                    <div class="form-group col-sm-6 col-xs-12">
                      <input class="form-control input-lg" type="text" name="name" placeholder="Name">
                    </div>
                    <!-- <div class="form-group col-sm-6 col-xs-12">
                      <select class="form-control input-lg" name="subject">
                        <option value="subject1" disabled="" selected="">Subject</option>
                        <option value="BusinessConsulting">Business consulting</option>
                        <option value="MarketingStrategy">Marketing Strategy</option>
                        <option value="TaxesAdvisory">Taxes Advisory</option>
                        <option value="InvestmentPlanning">Investment Planning</option>
                        <option value="ITManagement">IT Management</option>
                        <option value="DataAnalytics">Data Analytics</option>
                      </select>
                    </div> -->
                    <div class="form-group col-sm-12 col-xs-12">
                      <textarea class="form-control input-lg" name="" id="" cols="30" rows="10"></textarea>
                      <button class="btn btn-border-w btn-circle btn-block" id="racSubmit" type="submit"><i class="fa fa-paper-plane-o"></i> Submit</button>
                    </div>
                    <div id="requestFormResponse"></div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
        <hr class="divider-d">
        <footer class="footer bg-dark">
          <div class="container">
            <div class="row">
              <div class="col-sm-6">
                <p class="copyright font-alt">&copy; 2017&nbsp;<a href="index.html">TitaN</a>, All Rights Reserved</p>
              </div>
              <div class="col-sm-6">
                <div class="footer-social-links"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-dribbble"></i></a><a href="#"><i class="fa fa-skype"></i></a>
                </div>
              </div>
            </div>
          </div>
        </footer>
      </div>
      <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a></div>
      <script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK2Axt8xiFYMBMDwwG1XzBQvEbYpzCvFU"></script>
    </main>