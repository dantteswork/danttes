<section class="module pt-0 pb-0" id="login">
    <div class="row position-relative m-0">
        <div class="col-xs-12 col-md-4 side-image bg-gradient" data-background="https://images.unsplash.com/photo-1516321497487-e288fb19713f?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=cbbd27149a8243a42eb2bd0bd918ba8f&auto=format&fit=crop&w=1050&q=80"></div>
        <div class="col-xs-12 col-md-8 col-md-offset-4 side-image-text">
            <div class="row">
                <h3 class="font-alt mt-0" >Danttes</h3>
                <p>O Dantter ajuda você a se conectar e compartilhar com as pessoas que fazem parte da sua vida.</p>
                <hr class="divider-w mt-10 mb-20">
                <!-- <div class="font-alt">Create a stylish App Landing Page<br>And highlight all the great things of your product!</div> -->
                <form action="castro__associado" method="post">
                    <div class="col-xs-12 col-sm-12">
                        <h3 class="font-alt mb-0" style="margin-top: 0px;">Inscreva-se</h3>
                        <p>Crie sua conta, é gratuito e sempre será!</p>
                        <div class="form-group">
                            <label for="" class="font-alt mb-0">Nome</label>
                            <input class="form-control input-lg" name="nome" type="text" placeholder="Nome"/>
                        </div>
                        <div class="form-group">
                            <label for="" class="font-alt mb-0">Email ou Celular</label>
                            <input class="form-control input-lg"  name="email" type="text" placeholder="Email ou Celular"/>
                        </div>
                        <div class="form-group">
                            <label for="" class="font-alt mb-0">Cidade</label>
                            <input class="form-control input-lg"  name="cidade" type="text" placeholder="Cidade"/>
                        </div>
                        <div class="form-group">
                            <label for="" class="font-alt mb-0">Senha</label>
                            <input class="form-control input-lg" name="senha" type="password" placeholder="Senha"/>
                        </div>
                        <input type="hidden" name="id">
                        <input class="btn btn-info btn-round" type="submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>