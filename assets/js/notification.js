document.addEventListener('DOMContentLoaded', function(){
    if(!Notification)
    {
        alert('Navegador nao suporta notificações');
        return;
    }

    if(Notification.permission !== "granted")
    {
        Notification.requestPermission();
    }

    var title = 'Danttes Vagas';
    var icon = 'https://developers.google.com/web/images/web-fundamentals-icon192x192.png';
    var body = 'A mensagem da notificação';
    var link = 'www.google.com.br';

    function notifica(title, icon, body, link)
    {
        if(Notification.permission !== "granted")
        {
            Notification.requestPermission();
        }
        else
        {
            var notification = new Notification(title,{
                icon:icon,
                body:body
            });

            notification.onclik = function(){
                window.open(link);
            }
        }
    }
});